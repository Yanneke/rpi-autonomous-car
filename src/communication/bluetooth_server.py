import bluetooth


class BluetoothServer(object):
    def __init__(self, command_interpreter, port):
        # command to run to be discoverable: sudo hciconfig hci0 piscan
        self.interpreter = command_interpreter
        self.port = port
        self.server_sock = bluetooth.BluetoothSocket(bluetooth.L2CAP)

    def run(self):
        self.server_sock.bind(("", self.port))
        self.server_sock.listen(1)
        print("Waiting for a bluetooth connection on port %s..." % self.port)
        client_sock, address = self.server_sock.accept()
        print("Accepted connection from", address)
        data = client_sock.recv(1024)
        while data:
            self.interpreter.interpret(data)
            data = client_sock.recv(1024)

        client_sock.close()
        self.server_sock.close()
