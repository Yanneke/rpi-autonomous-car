import struct

# Commands
MOVE = b"move"
STOP = b"stop"
FORWARD = b"fwrd"
BACKWARD = b"bwrd"


class CommandBuilder(object):
    def __init__(self):
        pass

    def move(self, speed, direction):
        return MOVE + struct.pack(">2f", speed, direction)

    def stop(self):
        return STOP

    def forward(self, speed):
        return FORWARD + struct.pack(">f", speed)

    def backward(self, speed):
        return BACKWARD + struct.pack(">f", speed)



class MockInterpreter(object):
    def __init__(self):
        pass

    def interpret(self, bytes_data):
        params = bytes_data[4:]
        command = bytes_data[:4]
        print("len(%s)=%d" % (params, len(params)))
        if command == MOVE and len(bytes_data) == 12:
            speed, direction = struct.unpack(">2f", params)
            print("MOVE(speed:%f -- direction:%f)" % (speed, direction))
        elif command == STOP and len(bytes_data) == 4:
            print("STOP")
        elif command == FORWARD and len(bytes_data) == 8:
            speed = struct.unpack(">f", params)[0]
            print("FORWARD(speed:%f)" % speed)
        elif command == BACKWARD and len(bytes_data) == 8:
            speed = struct.unpack(">f", params)[0]
            print("BACKWARD(speed:%f)" % speed)
        else:
            print("Unknown or malformed command:", command)



class CommandInterpreter(object):
    def __init__(self, robot):
        self.robot = robot

    def interpret(self, bytes_data):
        params = bytes_data[4:]
        command = bytes_data[:4]
        if command == MOVE and len(bytes_data) == 12:
            speed, direction = struct.unpack(">2f", params)
            self.robot.motion.move(speed, direction)
        elif command == STOP and len(bytes_data) == 4:
            self.robot.motion.stop()
        elif command == FORWARD and len(bytes_data) == 8:
            speed = struct.unpack(">f", params)[0]
            self.robot.motion.forward(speed)
        elif command == BACKWARD and len(bytes_data) == 8:
            speed = struct.unpack(">f", params)[0]
            self.robot.motion.backward(speed)
        else:
            print("Unknown or malformed command:", command)
