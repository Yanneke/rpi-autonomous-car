import socket
import threading
import struct


class IPServer(object):
    def __init__(self, port, command_interpreter, camera):
        self._interpreter = command_interpreter
        self._port = port
        self._stop = False
        self._camera = camera

    def _receive_commands(self, sock: socket.socket):
        data = sock.recv(1024)
        while data:
            self._interpreter.interpret(data)
            data = sock.recv(1024)
        self._stop = True

    def _send_video_stream(self, sock: socket.socket):
        try:
            while not self._stop:
                frame = self._camera.get_jpeg_frame()
                image_data = frame.tobytes()
                img_len = struct.pack(">i", len(image_data))
                sock.sendall(img_len + image_data)
                ack = sock.recv(256)
                if ack != b"OK":
                    print("Error from the receiver")
                    break
        except (ConnectionResetError, BrokenPipeError):
            print("Connection lost")
        finally:
            print("End of video streaming")



    def run(self):
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_sock.bind(("", self._port))
        server_sock.listen()
        print("Waiting for a TCP connection on port %s..." % self._port)
        recv_sock, address = server_sock.accept()
        print("Connecting to socket for video stream...")
        video_sock = socket.create_connection((address[0], self._port + 1))
        print("Connected !")
        video_thread = threading.Thread(target=self._send_video_stream, args=(video_sock,))
        video_thread.start()

        self._receive_commands(recv_sock)
        video_thread.join()
        recv_sock.close()
        video_sock.close()
        server_sock.close()

