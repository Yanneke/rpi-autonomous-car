import socket
import struct
import threading
import cv2
import numpy as np
from time import time, sleep



class IPClient(object):
    def __init__(self, remote_host: str, sending_port: int, receiving_port: int):
        self._remote_host = remote_host
        self._remote_port = sending_port
        self._local_port = receiving_port
        self._stop = False
        self._server_socket = None
        self._send_socket = None
        self._video_thread = None

    def connect(self):
        # 1) Initialise port so that the remote server can connect to the video port
        self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server_socket.bind(("", self._local_port))
        self._server_socket.listen()
        # 2) Connect to the remote host
        self._send_socket = socket.create_connection((self._remote_host, self._remote_port))
        print("Connected to the remote host")
        # 3) Wait for the remote host to connect to the local port
        video_socket, _ = self._server_socket.accept()
        print("Accepted a remote connection")
        self._video_thread = threading.Thread(target=self._receive_video, args=[video_socket])
        self._video_thread.start()
        
    def disconnect(self):
        self._stop = True
        self._video_thread.join()
        self._server_socket.close()

    def _receive_video(self, video_socket):
        print(video_socket)
        window_name = "My window"
        cv2.namedWindow(window_name, cv2.WINDOW_AUTOSIZE)
        try:
            while not self._stop:
                data = video_socket.recv(4096)
                if not data:
                    continue
                img_len = struct.unpack(">i", data[:4])[0]
                image_data = data[4:]
                while len(image_data) < img_len:
                    image_data += video_socket.recv(4096)
                image_data = image_data[:img_len]
                video_socket.send(b"OK")
                image_data = np.frombuffer(image_data, dtype=np.uint8)
                image = cv2.imdecode(image_data, cv2.IMREAD_COLOR)
                cv2.imshow(window_name, image)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        finally:
            cv2.destroyAllWindows()
            video_socket.close()

    def send(self, bytes_data):
        if self._send_socket:
            self._send_socket.sendall(bytes_data)
        else:
            raise RuntimeError("The client is not connected! Data cannot be sent!")
