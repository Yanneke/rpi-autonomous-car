#! /usr/bin/env python3
import argparse
#from model import Robot
from communication import CommandInterpreter, MockInterpreter, IPServer
from sensors import Camera

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", dest="port", type=int, default=6666)
    args = parser.parse_args()
    
    #robot = Robot()
    camera = Camera()
    #interpreter = CommandInterpreter(robot)
    interpreter = MockInterpreter()
    server = IPServer(args.port, interpreter, camera)
    server.run()
