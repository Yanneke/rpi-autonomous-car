#! /usr/bin/env python3
from communication import IPClient
from controllers import GamePadController
import argparse




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", dest="port", type=int, default=6666)
    parser.add_argument("--host", dest="remote_host", type=str, default="topaze.local")
    args = parser.parse_args()

    client = IPClient(args.remote_host, args.port, args.port + 1)
    client.connect()
    gamepad = GamePadController(client)
    gamepad.start()
    client.disconnect()
    