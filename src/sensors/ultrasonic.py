import time
from gpiozero import PWMOutputDevice, DigitalOutputDevice, DigitalInputDevice

class UltrasonicSensor(object):
    SOUND_SPEED_MS = 340
    SOUND_SPEED_HALF_MS = 170

    def __init__(self):
        self._trigger = DigitalOutputDevice("BOARD28")
        self._echo = DigitalInputDevice("BOARD3")

    def sense(self):
        self._trigger.on()
        time.sleep(0.000015)
        self._trigger.off()
        while not self._echo.is_active:
            pass
        start = time.time()
        while self._echo.is_active:
            pass
        duration = time.time() - start
        return duration * UltrasonicSensor.SOUND_SPEED_HALF_MS
