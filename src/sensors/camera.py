import cv2


class Camera(object):
    def __init__(self):
        self.cap = cv2.VideoCapture(0)


    def get_raw_frame(self):
        _, frame = self.cap.read()
        return frame

    def get_jpeg_frame(self):
        _, frame = self.cap.read()
        frame = cv2.flip(frame, -1)
        _, data = cv2.imencode('.jpg', frame)
        return data

    def __del__(self):
        self.cap.release()