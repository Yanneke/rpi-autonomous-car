from gpiozero import PWMOutputDevice, DigitalOutputDevice
from math import pi

class Motion(object):
    
    def __init__(self, pwm_freq=2000):
        self.LEFT_BACK = DigitalOutputDevice("BOARD40")
        self.LEFT_FWD = DigitalOutputDevice("BOARD38")
        self.PWM_LEFT = PWMOutputDevice("BOARD36", frequency=pwm_freq)
        self.RIGHT_BACK = DigitalOutputDevice("BOARD37")
        self.RIGHT_FWD = DigitalOutputDevice("BOARD35")
        self.PWM_RIGHT = PWMOutputDevice("BOARD33", frequency=pwm_freq)

    def stop(self):
        """
        Stop the motors.
        :return: None
        """
        self.LEFT_BACK.off()
        self.LEFT_FWD.off()
        self.RIGHT_BACK.off()
        self.RIGHT_FWD.off()
        self.PWM_LEFT.off()
        self.PWM_RIGHT.off()
    

    def move(self, speed, direction):
        """
        Steer the robot to the given angle.
        @param:speed [float]: the speed in percentage. Should be between 0 and 1.
        @param:direction [float]: the direction balance [-1, 1] where -1 is maximum left and 1 is maximum right
        @return: None
        """
        if direction > 0: # Right
            self._actuate_right(speed * (1 - direction))
            self._actuate_left(speed)
        elif direction < 0: # Left
            self._actuate_right(speed)
            self._actuate_left(speed * (1 + direction))
        else: # Straight forward
            self._actuate_right(speed)
            self._actuate_left(speed)
            


    def spin_left(self, speed):
        """
        Spin left.
        """
        self.LEFT_BACK.off()
        self.LEFT_FWD.on()
        self.RIGHT_BACK.on()
        self.RIGHT_FWD.off()
        self.PWM_LEFT.value = speed
        self.PWM_RIGHT.value = speed

    def spin_right(self, speed):
        """
        Spin right.
        """
        self.LEFT_BACK.on()
        self.LEFT_FWD.off()
        self.RIGHT_BACK.off()
        self.RIGHT_FWD.on()
        self.PWM_LEFT.value = speed
        self.PWM_RIGHT.value = speed

    
    def forward(self, speed):
        """
        Steer the robot straight forward at the given speed.
        @speed [float]: the speed in percentage. Should be between 0 and 1.
        @return: None
        """
        self.LEFT_BACK.on()
        self.LEFT_FWD.off()
        self.RIGHT_BACK.on()
        self.RIGHT_FWD.off()
        self.PWM_LEFT.value = speed
        self.PWM_RIGHT.value = speed



    def backward(self, speed):
        """
        Steer the robot straight backward at the given speed.
        @speed [float]: the speed in percentage. Should be between 0 and 1.
        @return: None
        """
        self.LEFT_BACK.off()
        self.RIGHT_BACK.off()
        self.LEFT_FWD.on()
        self.RIGHT_FWD.on()
        self.PWM_LEFT.value = speed
        self.PWM_RIGHT.value = speed

    def _actuate_left(self, speed):
        """
        Actuates left wheels at the given speed. Negative speed rotates backward.
        """
        if speed >= 0:
            self.LEFT_BACK.off()
            self.LEFT_FWD.on()
        else:
            self.LEFT_BACK.on()
            self.LEFT_FWD.off()
        #self.LEFT_PWM.value = speed
        self.PWM_LEFT.value = abs(speed)

    def _actuate_right(self, speed):
        """
        Actuates right wheels at the given speed. Negative speed rotates backward.
        """
        if speed >= 0:
            self.RIGHT_BACK.off()
            self.RIGHT_FWD.on()
        else:
            self.RIGHT_BACK.on()
            self.RIGHT_FWD.off()
        self.PWM_RIGHT.value = abs(speed)