from actuators import Motion
from sensors import UltrasonicSensor, Camera, LightSensor, LineFollower 


class Robot(object):
    def __init__(self):
        self.motion = Motion()
        self.radar = UltrasonicSensor()
        self.line_follower = LineFollower()
        self.light_sensor = LightSensor()
        self.camera = Camera()

    
