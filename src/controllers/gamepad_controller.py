import inputs
from math import pi, atan2
from communication.commands import CommandBuilder

class GamePadController(object):
    def __init__(self, client, gamepad_num=0):
        self._client = client
        if len(inputs.devices.gamepads) < gamepad_num + 1:
            raise inputs.UnpluggedError("Gamepad device #%d not detected!" % gamepad_num)
        self._gamepad = inputs.devices.gamepads[gamepad_num]
        self._command_builder = CommandBuilder()

    def start(self):
        try:
            speed = 0
            direction = 0
            while True:
                for event in self._gamepad.read():
                    print(event.ev_type, event.code, event.state)
                    if event.code == "ABS_RX":
                        direction = round(event.state / 32768, 2)
                        if -0.05 < direction < 0.05:
                            direction = 0
                        elif direction < 0:
                            direction = -(direction ** 2)
                        else:
                            direction = direction ** 2
                        cmd = self._command_builder.move(speed, direction)
                        self._client.send(cmd)
                    elif event.code == "ABS_Y":
                        speed = round(event.state / -32768, 2)
                        if -0.05 < speed < 0.05:
                            speed = 0
                        elif speed < 0:
                            speed = -(speed ** 2)
                        else:
                            speed = speed ** 2
                        cmd = self._command_builder.move(speed, direction)
                        self._client.send(cmd)
                    elif event.code == "BTN_START" and event.state == 1:
                        raise KeyboardInterrupt("Gamepad start button")
        except KeyboardInterrupt as e:
            reason = str(e)
            error_message = "Interrupt signal caught."
            if reason:
                error_message += " Reason: %s" % reason
            print(error_message)
        

        
